let form = document.querySelector(".password-form");
let message = document.querySelector(".error-message");

function showPassword() {
    let icon = this.event.target;
    icon.classList.toggle("fa-eye-slash");
    let password = icon.closest(".input-wrapper").querySelector(".password");
    const type = password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
}

form.addEventListener("click", function (event) {
    if (event.target.closest(".icon-password")) {
        showPassword()
    }
})

form.addEventListener("submit", function (event) {
    event.preventDefault();
    let firstLinePassword = document.querySelector(".first-password");
    let secondLinePassword = document.querySelector(".confirm-password");

    if (firstLinePassword.value !== secondLinePassword.value || firstLinePassword.value === "") {
        message.textContent = "Потрібно ввести однакові значення"
    } else {
        message.textContent = null
        alert("You are welcome!");
    }
});










